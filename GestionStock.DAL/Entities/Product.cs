﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL.Entities
{
    public class Product
    {
        public int Id { get; set; }

        [MaxLength(8)]
        public string Reference { get; set; }

        [MaxLength(50), MinLength(4)]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        public int Stock { get; set; }

        [Column(TypeName = "decimal(6, 2)")]
        public decimal Price { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

    }
}
