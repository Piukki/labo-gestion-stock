﻿using GestionStock.DAL.Configs;
using GestionStock.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionStock.DAL
{
    public class GestionStockDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public GestionStockDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ProductConfig());
            builder.Entity<Product>().Property(o => o.Price).HasPrecision(6, 2);
        }
    }
}
