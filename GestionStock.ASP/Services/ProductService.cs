﻿using GestionStock.DAL;
using GestionStock.DAL.Entities;

namespace GestionStock.ASP.Services
{
    public class ProductService
    {
        private readonly GestionStockDbContext _dc;

        public ProductService(GestionStockDbContext dc)
        {
            _dc = dc;
        }

        public IEnumerable<Product> GetProducts()
        {
            return _dc.Products.Where(pr => pr.IsDeleted == false)
                .OrderBy(p => p.Reference)
                .Take(20);
        }

        public void CreateProduct(Product p)
        {
            //Règles métiers
            p.CreationDate = DateTime.Now;
            p.UpdateDate = DateTime.Now;
            //Pour créer la référence
            string firstLetters = p.Name.Replace(" ", "").Substring(0, 4);
            int nb = _dc.Products.Where(pr => pr.Reference.StartsWith(firstLetters)).Count() +1;
            p.Reference = firstLetters + nb.ToString().PadLeft(4, '0');

            //Enregistrer dans la db
            _dc.Products.Add(p);
            _dc.SaveChanges();
        }
    }
}
