﻿namespace GestionStock.ASP.Models
{
    public class ProductCreateModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int Stock { get; set; }
    }
}
