﻿namespace GestionStock.ASP.Models
{
    public class ProductViewModel
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Reference { get; set; }
    }
}
