﻿namespace GestionStock.ASP.Exceptions
{
    public class ModelException : Exception
    {
        public string FieldName { get; set; }

        public ModelException(string fieldName, string message) : base(message)
        {
            FieldName = fieldName;
        }
    }
}
