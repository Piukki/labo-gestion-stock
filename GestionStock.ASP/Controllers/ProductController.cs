﻿using GestionStock.ASP.Models;
using GestionStock.ASP.Services;
using GestionStock.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace GestionStock.ASP.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductService _productService;

        public ProductController(ProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            IEnumerable<Product> products = _productService.GetProducts();
            IEnumerable<ProductViewModel> model = products
                .Select( p => new ProductViewModel { 
            
                Name = p.Name,
                Price = p.Price,
                Reference = p.Reference,
            });
            return View(model);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ProductCreateModel model)
        {
            if (ModelState.IsValid)
            {
                Product p = new Product
                {
                    Name = model.Name,
                    Price = model.Price,
                    Description = model.Description,
                    Stock = model.Stock
                };

            _productService.CreateProduct(p);
            return RedirectToAction(nameof(Index));
            }
            return View();
        }

    }
}
